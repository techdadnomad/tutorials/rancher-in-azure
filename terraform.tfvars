# Required variables
# - Fill in before beginning quickstart
# ==========================================================

# Azure Subscription ID
azure_subscription_id = "fe827559-4431-4d79-a23f-3f2326be3cea"

# Azure Client ID
azure_client_id = "a8bb5838-cef2-415c-bd8c-007576b574a0"

# Azure Client Secret
azure_client_secret = "8L6LpoXK~36ZR~UR__TpLsn-f2my6NR_eC"

# Azure Tenant ID
azure_tenant_id = "33a80c43-c6db-4af6-ab48-af1b788748be"

# Password used to log in to the `admin` account on the new Rancher server
rancher_server_admin_password = "ChangeMe"

# Optional variables, uncomment to customize the quickstart
# ----------------------------------------------------------

# Azure location for all resources
azure_location = "Central US" # defaulted to 'East US'

# Prefix for all resources created by quickstart
prefix = "wsl-vagrant"

# Azure virtual machine instance size of all created instances
instance_type = "Standard_D4_v2"

# Docker version installed on target hosts
# - Must be a version supported by the Rancher install scripts
# docker_version = ""

# Kubernetes version used for creating management server cluster
# - Must be supported by RKE terraform provider 1.0.1
# rke_kubernetes_version = ""

# Kubernetes version used for creating workload cluster
# - Must be supported by RKE terraform provider 1.0.1
# workload_kubernetes_version = ""

# Version of cert-manager to install, used in case of older Rancher versions
# cert_manager_version = ""

# Version of Rancher to install
# rancher_version = ""
